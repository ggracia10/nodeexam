var express = require("express");
var app = express();
var bodyParser = require('body-parser');
var request = require('request');

const readChunk = require('read-chunk');
const fileType = require('file-type');

app.use(bodyParser.json())

var wordsList = [];
var orderArray = [];
var lastURL;

//##############################################################################
//                                  EXC2
//##############################################################################

app.post('/removeduplicatewords', function(req,res){
    wordsList = [];
    var tempWordsList = req.body.palabras;
    var returnString = "";
    wordsList = AddWordsToList(req.body.palabras);
    for(let i = 0; i < wordsList.length; i++) {
        returnString += wordsList[i];
        if(i < wordsList.length-1){
            returnString += ',';
        }
    }
    res.end(returnString);
});

function AddWordsToList(tempWordsList) {
    var wordsList = [];
    var tempString = "";
    //Añadimos todas las palabras en una lista
    for(let i = 0; i < tempWordsList.length; i++){
        if(tempWordsList.charAt(i) == ',' || tempWordsList.charAt(i) == '\0') {
            if(!CheckForRepeatedWords(tempString, wordsList)) {
                wordsList.push(tempString);
            }
            tempString = "";
        }
        else {
            tempString+=tempWordsList.charAt(i);
        }
    }
    if(!CheckForRepeatedWords(tempString, wordsList)){
        wordsList.push(tempString);
    }
    return wordsList;
}

function CheckForRepeatedWords(word, list) {
    
    if(list.length == 0) return false;

    for(let i = 0; i < list.length; i++) {
        if(word == list[i]){
            return true;
        }
    }
    return false;
}

//##############################################################################
//                                  EXC3
//##############################################################################

app.post('/detectfiletype', function(req,res) {
    var myUrl = req.body.url;
    if(lastURL == myUrl) return;
    lastURL = myUrl;

    const https = require('https');
    const fileType = require('file-type');
    
    https.get(myUrl, response => {
        response.on('readable', () => {
            var chunk = response.read(fileType.minimumBytes);
            response.destroy();
            var string = JSON.stringify(fileType(chunk));
            console.log(string);
            res.end(string);
            //=> {ext: 'gif', mime: 'image/gif'}
        });
    });


});

//##############################################################################
//                                  EXC4
//##############################################################################
app.get('/botorder/:order', function(req,res) {

    var orderNum = req.params.order.substring(5, req.params.order.length);
    
    if(orderArray[orderNum]) {
        res.end(orderArray[orderNum]);
        console.log(orderArray[orderNum]);
    }
    else {
        res.end("NONE");
        console.log("NONE");
    }
});

app.post('/botorder/:order', function(req,res) {
    var orderNum = req.params.order.substring(5, req.params.order.length);
    orderArray[orderNum] = req.body.botorder;
    console.log("Added: " + orderArray[orderNum] + " on ORDER" + orderNum);
    res.end("OK");
});

app.listen(8000);